# AI Utilities
Provides several small utilities that are useful for AI features provided by other modules, and could also be useful for other types of features.

- Check if a string is HTML formatted
- Convert Markdown to HTML
- Remove a prefix and suffix, and everything before and after to get just the content inbetween


For a full description of the module, visit the [project page](https://www.drupal.org/project/ai_utilities).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/ai_utilities).



## Requirements

[league/commonmark](https://github.com/thephpleague/commonmark) handles Markdown to HTML conversion and is automatically installed when this module is added with Composer


## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

None.


## Maintainers

- Robert Castelo - [robert-castelo](https://www.drupal.org/u/robert-castelo)