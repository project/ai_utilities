<?php

namespace Drupal\ai_utilities;

/**
 * Utilities for cleaning up AI output.
 */
interface FormatInterface {

  /**
   * Check if a string is HTML formatted.
   */
  public static function isHtml(string $string);

  /**
   * Convert Markdown to HTML.
   */
  public static function markdownToHtml(string $string);

  /**
   * Remove the prefix and suffix OpenAI tends to add.
   */
  public static function trim(string $message, string $prefix = "```html", string $suffix = "```");

}
