<?php

namespace Drupal\ai_utilities;

use League\CommonMark\CommonMarkConverter;

/**
 * Utilities for formatting AI output.
 */
class Format implements FormatInterface {

  /**
   * Check if a string is HTML formatted.
   */
  public static function isHtml($string) {
    // Markdown can include HTML comments so remove them before evaluating if
    // rest of string is HTML.
    $string = preg_replace('/<!--(.|\s)*?-->/', '', $string);
    return ($string != strip_tags($string)) ? TRUE : FALSE;
  }

  /**
   * Convert Mardown to HTML.
   */
  public static function markdownToHtml($string) {
    $config = [
      'html_input' => 'allow',
      'allow_unsafe_links' => TRUE,
    ];

    $converter = new CommonMarkConverter($config);
    $converted = $converter->convert($string);
    $content = is_string($converted) ? $converted : $converted->getContent();
    return $content;
  }

  /**
   * Remove the prefix and suffix OpenAI tends to add.
   */
  public static function trim($message, $prefix = "```html", $suffix = "```") {
    // Remove preffix and everything before it.
    $position = strpos($message, $prefix);

    if ($position !== FALSE) {
      $message = substr($message, $position + strlen($prefix));
    }

    // Remove suffix and everything that comes after it.
    $position = strpos($message, $suffix);

    if ($position !== FALSE) {
      $message = strstr($message, $suffix, TRUE);
    }

    return $message;
  }

}
